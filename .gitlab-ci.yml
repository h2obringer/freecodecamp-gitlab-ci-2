stages:
    - build
    - package
    - test
    - deploy

variables:
    APP_VERSION: $CI_PIPELINE_IID

build website:
    stage: build
    image: node:16.14.2-alpine3.15
    script:
        - yarn install
        - yarn lint
        - yarn test
        - yarn build
        - echo $APP_VERSION > build/version.html
    artifacts:
        paths:
            - build

build docker image:
  stage: package
  image: docker:20.10.14
  services:
    - docker:20.10.14-dind-alpine3.15
  script:
    # - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY #allows us to login with temp user/pass to the GitLab Private Container Registry for our Project, passing the password directly to the command which might become deprecated soon
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin #allows us to login with temp user/pass to the GitLab Private Container Registry for our Project where we pipe the password to the docker standard input, supposed to be safer and more secure
    - docker build . -t $CI_REGISTRY_IMAGE -t $CI_REGISTRY_IMAGE:$APP_VERSION #builds 2 images, 1 with the :latest tag, and 1 with a version tag
    - docker image ls
    - docker push --all-tags $CI_REGISTRY_IMAGE #persist the images we just built to the GitLab Private Container Registry for our Project

test docker image:
  stage: test
  image: curlimages/curl
  services:
    - name: $CI_REGISTRY_IMAGE:$APP_VERSION #runs our current image as a service, giving the website running in the container an alias called "website" so we can reference it within the script
      alias: website
  script:
    - curl http://website/version.html | grep $APP_VERSION

deploy to production:
  stage: deploy
  image:
    name: amazon/aws-cli:2.4.11
    entrypoint: [""]
  environment: PRODUCTION
  variables:
    APP_NAME: freecodecamp-cicd2 #the name of the elastic bean stalk application
    APP_ENV_NAME: Freecodecampcicd2-env
  script:
    - aws --version
    - yum install -y gettext #yum is a package manager, use -y to answer any install questions with yes, gettext is required in order to use the envsubst command below
    - export DEPLOY_TOKEN=$(echo $GITLAB_DEPLOY_TOKEN | tr -d "\n" | base64) #AWS command expects the deployment token as a base64 string. pipe the token variable value to tr which will translate and remove newline characters then pipe that to the base64 command to convert it to base64. Store the result to the DEPLOY_TOKEN variable (exported from the script)
    - envsubst < templates/Dockerrun.aws.json > Dockerrun.aws.json #replaces variables within file with environment variables from GitLab, taking templates/Dockerrun.aws.json as input and outputting the file to the current directory
    - envsubst < templates/auth.json > auth.json #replaces variables within file with environment variables from GitLab, taking templates/Dockerrun.aws.json as input and outputting the file to the current directory
    - cat Dockerrun.aws.json #show the contents of this file in the job log so we can verify the envsubst command worked correctly
    - cat auth.json #show the contents of this file in the job log so we can verify the envsubst command worked correctly
    - aws s3 cp Dockerrun.aws.json s3://$AWS_S3_BUCKET/Dockerrun.aws.json
    - aws s3 cp auth.json s3://$AWS_S3_BUCKET/auth.json
    - aws elasticbeanstalk create-application-version --application-name "$APP_NAME" --version-label $APP_VERSION --source-bundle S3Bucket=$AWS_S3_BUCKET,S3Key=Dockerrun.aws.json
    - aws elasticbeanstalk update-environment --application-name "$APP_NAME" --version-label $APP_VERSION --environment-name $APP_ENV_NAME
    - aws elasticbeanstalk wait environment-updated --application-name "$APP_NAME" --version-label $APP_VERSION --environment-name $APP_ENV_NAME
    - curl $CI_ENVIRONMENT_URL/version.html | grep $APP_VERSION